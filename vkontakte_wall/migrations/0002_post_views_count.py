# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vkontakte_wall', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='views_count',
            field=models.PositiveIntegerField(null=True, verbose_name='\u041a\u043e\u043b-\u0432\u043e \u043f\u0440\u043e\u0441\u043c\u043e\u0442\u0440\u043e\u0432'),
            preserve_default=True,
        ),
    ]
