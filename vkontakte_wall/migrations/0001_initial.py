# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import m2m_history.fields
import vkontakte_api.models
import vkontakte_api.fields


class Migration(migrations.Migration):

    dependencies = [
        ('vkontakte_users', '0004_auto_20170411_0048'),
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fetched', models.DateTimeField(db_index=True, null=True, verbose_name='\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e', blank=True)),
                ('remote_id', models.CharField(help_text='\u0423\u043d\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0439 \u0438\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440', unique=True, max_length=20, verbose_name='ID')),
                ('archived', models.BooleanField(default=False, verbose_name='\u0412 \u0430\u0440\u0445\u0438\u0432\u0435')),
                ('author_id', models.BigIntegerField(null=True, db_index=True)),
                ('owner_id', models.BigIntegerField(null=True, db_index=True)),
                ('likes_count', models.PositiveIntegerField(null=True, verbose_name='Likes', db_index=True)),
                ('raw_json', vkontakte_api.fields.JSONField(default={}, null=True)),
                ('comments_count', models.PositiveIntegerField(help_text=b'The number of comments of this item', null=True, verbose_name=b'Comments')),
                ('reposts_count', models.PositiveIntegerField(null=True, verbose_name='\u041a\u043e\u043b-\u0432\u043e \u0440\u0435\u043f\u043e\u0441\u0442\u043e\u0432', db_index=True)),
                ('raw_html', models.TextField()),
                ('date', models.DateTimeField(verbose_name='\u0412\u0440\u0435\u043c\u044f \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f', db_index=True)),
                ('text', models.TextField(verbose_name='\u0422\u0435\u043a\u0441\u0442 \u0437\u0430\u043f\u0438\u0441\u0438')),
                ('attachments', models.TextField()),
                ('media', models.TextField()),
                ('geo', models.TextField()),
                ('signer_id', models.PositiveIntegerField(help_text='E\u0441\u043b\u0438 \u0437\u0430\u043f\u0438\u0441\u044c \u0431\u044b\u043b\u0430 \u043e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d\u0430 \u043e\u0442 \u0438\u043c\u0435\u043d\u0438 \u0433\u0440\u0443\u043f\u043f\u044b \u0438 \u043f\u043e\u0434\u043f\u0438\u0441\u0430\u043d\u0430 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u0435\u043c, \u0442\u043e \u0432 \u043f\u043e\u043b\u0435 \u0441\u043e\u0434\u0435\u0440\u0436\u0438\u0442\u0441\u044f \u0438\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440 \u0435\u0451 \u0430\u0432\u0442\u043e\u0440\u0430', null=True)),
                ('copy_owner_id', models.PositiveIntegerField(help_text='E\u0441\u043b\u0438 \u0437\u0430\u043f\u0438\u0441\u044c \u044f\u0432\u043b\u044f\u0435\u0442\u0441\u044f \u043a\u043e\u043f\u0438\u0435\u0439 \u0437\u0430\u043f\u0438\u0441\u0438 \u0441 \u0447\u0443\u0436\u043e\u0439 \u0441\u0442\u0435\u043d\u044b, \u0442\u043e \u0432 \u043f\u043e\u043b\u0435 \u0441\u043e\u0434\u0435\u0440\u0436\u0438\u0442\u0441\u044f \u0438\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440 \u0432\u043b\u0430\u0434\u0435\u043b\u044c\u0446\u0430 \u0441\u0442\u0435\u043d\u044b \u0443 \u043a\u043e\u0442\u043e\u0440\u043e\u0433\u043e \u0431\u044b\u043b\u0430 \u0441\u043a\u043e\u043f\u0438\u0440\u043e\u0432\u0430\u043d\u0430 \u0437\u0430\u043f\u0438\u0441\u044c', null=True, db_index=True)),
                ('copy_text', models.TextField(help_text='\u0415\u0441\u043b\u0438 \u0437\u0430\u043f\u0438\u0441\u044c \u044f\u0432\u043b\u044f\u0435\u0442\u0441\u044f \u043a\u043e\u043f\u0438\u0435\u0439 \u0437\u0430\u043f\u0438\u0441\u0438 \u0441 \u0447\u0443\u0436\u043e\u0439 \u0441\u0442\u0435\u043d\u044b \u0438 \u043f\u0440\u0438 \u0435\u0451 \u043a\u043e\u043f\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u0438 \u0431\u044b\u043b \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d \u043a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439, \u0435\u0433\u043e \u0442\u0435\u043a\u0441\u0442 \u0441\u043e\u0434\u0435\u0440\u0436\u0438\u0442\u0441\u044f \u0432 \u0434\u0430\u043d\u043d\u043e\u043c \u043f\u043e\u043b\u0435', verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439 \u043f\u0440\u0438 \u0440\u0435\u043f\u043e\u0441\u0442\u0435')),
                ('post_source', models.TextField()),
                ('online', models.PositiveSmallIntegerField(null=True)),
                ('reply_count', models.PositiveIntegerField(null=True)),
                ('author_content_type', models.ForeignKey(related_name='content_type_authors_vkontakte_wall_posts', to='contenttypes.ContentType', null=True)),
                ('copy_owner_content_type', models.ForeignKey(related_name='vkontakte_wall_copy_posts', to='contenttypes.ContentType', null=True)),
                ('copy_post', models.ForeignKey(related_name='wall_reposts', to='vkontakte_wall.Post', help_text='\u0415\u0441\u043b\u0438 \u0437\u0430\u043f\u0438\u0441\u044c \u044f\u0432\u043b\u044f\u0435\u0442\u0441\u044f \u043a\u043e\u043f\u0438\u0435\u0439 \u0437\u0430\u043f\u0438\u0441\u0438 \u0441 \u0447\u0443\u0436\u043e\u0439 \u0441\u0442\u0435\u043d\u044b, \u0442\u043e \u0432 \u043f\u043e\u043b\u0435 \u0441\u043e\u0434\u0435\u0440\u0436\u0438\u0442\u0441\u044f \u0438\u0434\u0435\u043d\u0442\u0444\u0438\u043a\u0430\u0442\u043e\u0440 \u0441\u043a\u043e\u043f\u0438\u0440\u043e\u0432\u0430\u043d\u043d\u043e\u0439 \u0437\u0430\u043f\u0438\u0441\u0438 \u043d\u0430 \u0441\u0442\u0435\u043d\u0435 \u0435\u0435 \u0432\u043b\u0430\u0434\u0435\u043b\u044c\u0446\u0430', null=True)),
                ('likes_users', m2m_history.fields.ManyToManyHistoryField(related_name='like_posts', to='vkontakte_users.User')),
                ('owner_content_type', models.ForeignKey(related_name='content_type_owners_vkontakte_wall_posts', to='contenttypes.ContentType', null=True)),
                ('reposts_users', m2m_history.fields.ManyToManyHistoryField(related_name='reposts_posts', to='vkontakte_users.User')),
            ],
            options={
                'verbose_name': '\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435 \u0412\u043a\u043e\u043d\u0442\u0430\u043a\u0442\u0435',
                'verbose_name_plural': '\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f \u0412\u043a\u043e\u043d\u0442\u0430\u043a\u0442\u0435',
            },
            bases=(vkontakte_api.models.RemoteIdModelMixin, models.Model),
        ),
    ]
